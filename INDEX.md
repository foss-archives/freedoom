# Freedoom

SMMU is a Doom source port derived from MBF and Boom. This version is packaged with Freedoom Phase 1 with 4 chapters containing 9 levels each and Phase 2 consisting of a single large 32 chapter level.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FREEDOOM.LSM

<table>
<tr><td>title</td><td>Freedoom</td></tr>
<tr><td>version</td><td>3.30b</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-02-11</td></tr>
<tr><td>description</td><td>Doom source port derived from MBF and Boom</td></tr>
<tr><td>keywords</td><td>doom, boom, Freedoom</td></tr>
<tr><td>author</td><td>Simon Howard, Lee Killough, John Carmack</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.soulsphere.org</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.nongnu.org/freedoom/, http://free.doomers.org, http://savannah.nongnu.org/projects/freedoom</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.soulsphere.org, http://freedoom.sf.net</td></tr>
<tr><td>platforms</td><td>DOS, Win32, Linux</td></tr>
<tr><td>copying&nbsp;policy</td><td>Open source, see Licenses</td></tr>
<tr><td>summary</td><td>SMMU is a Doom source port derived from MBF and Boom. This version is packaged with Freedoom Phase 1 with 4 chapters containing 9 levels each and Phase 2 consisting of a single large 32 chapter level.</td></tr>
</table>
